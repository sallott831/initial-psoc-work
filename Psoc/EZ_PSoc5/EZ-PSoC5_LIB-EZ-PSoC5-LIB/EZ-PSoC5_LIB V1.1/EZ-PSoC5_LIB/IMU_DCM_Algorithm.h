/* ========================================
 *
 * Copyright iAppSpace.com, 2013
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#if !defined(IMU_DCM_ALGORITHM_H)
#define IMU_DCM_ALGORITHM_H

#include <cytypes.h>
#include "TypeDefine.h"
#include "ezCOMM.h"
//------------------------------------------------------------
// Defines ///////////////////////////////////////////////////
//------------------------------------------------------------
/*For debugging purposes*/
//OUTPUTMODE=1 will print the corrected data,
//OUTPUTMODE=0 will print uncorrected data of the gyros (with drift)
#define IMUDCM_OUTPUTMODE 1


//#define PRINT_DCM 0     //Will print the whole direction cosine matrix
typedef enum IMUDCM_OUTPUT_FORMAT {
    IMUDCM_ANALOGS = 0, //Will print the analog raw data
    IMUDCM_EULER        //Will print the Euler angles Roll, Pitch and Yaw
} IMUDCM_OUTPUT_FORMAT;

//------------------------------------------------------------
// Public Data Structure /////////////////////////////////////
//------------------------------------------------------------
typedef struct EZOBJ_IMUDCM {
    void*       PrivateData;

    float       Kp_ROLLPITCH;   //      0.02f
    float       Ki_ROLLPITCH;   //      0.00002f
    float       Kp_YAW;         //      1.2f
    float       Ki_YAW;         //      0.00002f
    
    float       roll;
    float       pitch;
    float       yaw;
    float       MagHeading;
    #ifdef DEBUG
        float   G_Dt;
    #endif
} EZOBJ_IMUDCM;

typedef EZOBJ_IMUDCM * PEZOBJ_IMUDCM;

//------------------------------------------------------------
// Public Functions //////////////////////////////////////////
//------------------------------------------------------------

PEZOBJ_IMUDCM      IMUDCM_Create();
void                IMUDCM_Release(PEZOBJ_IMUDCM dcm);

void                IMUDCM_ConnectISR(PEZOBJ_IMUDCM dcm, PEZOBJ_ISR isr);

void                IMUDCM_PlugGyroIF(PEZOBJ_IMUDCM dcm, PEZOBJ_INTERFACE gyro);
void                IMUDCM_PlugAccIF (PEZOBJ_IMUDCM dcm, PEZOBJ_INTERFACE acc);
void                IMUDCM_PlugMagIF (PEZOBJ_IMUDCM dcm, PEZOBJ_INTERFACE msg);

void                IMUDCM_SetOutputFormat(PEZOBJ_IMUDCM dcm, IMUDCM_OUTPUT_FORMAT format);

bool                IMUDCM_Init (PEZOBJ_IMUDCM dcm);
void                IMUDCM_Start(PEZOBJ_IMUDCM dcm);

bool               IMUDCM_MeasureCurrentPositions(PEZOBJ_IMUDCM dcm);

//------------------------------------------------------------
// Sample Code and Parameters ////////////////////////////////
//------------------------------------------------------------
/*
    PEZOBJ_IMUDCM  imu;

    intfGyro = INTERFACE_Create();
    intfAcc  = INTERFACE_Create();
    intfMag  = INTERFACE_Create();
//
//
//    L3G_GetGyroIntf(l3g, intfGyro);
//    LSM303_GetAccIntf(lsm303, intfAcc);
//    LSM303_GetMagIntf(lsm303, intfMag);

    MPU9150_GetGyroIntf(mpu, intfGyro);
    MPU9150_GetAccIntf (mpu, intfAcc);
    MPU9150_GetMagIntf (mpu, intfMag);

    imu = IMUDCM_Create();

    IMUDCM_PlugGyroIF(imu, intfGyro);
    IMUDCM_PlugAccIF (imu, intfAcc);
    IMUDCM_PlugMagIF (imu, intfMag);

    IMUDCM_Init(imu);
    IMUDCM_Start(imu);

    IMUDCM_MeasureCurrentPositions(imu);


*/
#endif
//[] END OF FILE
