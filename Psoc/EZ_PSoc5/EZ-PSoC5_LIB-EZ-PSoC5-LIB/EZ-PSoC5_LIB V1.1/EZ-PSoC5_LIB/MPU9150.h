/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
/*
 * Chip Number : MPU-9150
 * Description : 9 Degrees of Freedom
 * Manufacture : InvenSense (Sparkfun)
 * Web Site    : https://www.sparkfun.com/products/11486
 * Interfaces  : I2C
*/

#ifndef MPU_9150_H
#define MPU_9150_H

#include "ezCOMM.h"
#include <cytypes.h>
#include "TypeDefine.h"

//------------------------------------------------------------
// Defines ///////////////////////////////////////////////////
//------------------------------------------------------------

typedef enum{
    MPU9150_AD0_A_LOW                   = 0,
    MPU9150_AD0_A_HIGH                  = 1,
    MPU9150_AD0_A_AUTO                  = 2
} MPU9150_CONF_AD0_STATUS;

//typedef enum {
//    MPU9150_DLPF_256_HZ                 = 0x00,
//    MPU9150_DLPF_188_HZ                 = 0x01,
//    MPU9150_DLPF_98_HZ                  = 0x02,
//    MPU9150_DLPF_42_HZ                  = 0x03,
//    MPU9150_DLPF_20_HZ                  = 0x04,
//    MPU9150_DLPF_10_HZ                  = 0x05,
//    MPU9150_DLPF_5_HZ                   = 0x06
//} MPU9150_CONF_DLP_FILTER;

typedef enum {
    MPU9150_GYRO_FULLSCALE_250DPS       = 0x00,
    MPU9150_GYRO_FULLSCALE_500DPS       = 0x08,
    MPU9150_GYRO_FULLSCALE_1000DPS      = 0x10,
    MPU9150_GYRO_FULLSCALE_2000DPS      = 0x18
} MPU9150_CONF_GYRO_FULLSCALE;

typedef enum {
    MPU9150_ACC_FULLSCALE_2G            = 0x00,
    MPU9150_ACC_FULLSCALE_4G            = 0x08,
    MPU9150_ACC_FULLSCALE_8G            = 0x10,
    MPU9150_ACC_FULLSCALE_16G           = 0x18
} MPU9150_CONF_ACC_FULLSCALE;

//typedef enum {
//    MPU9150_ACC_HPF_RESET               = 0x00,
//    MPU9150_ACC_HPF_5_HZ                = 0x01,
//    MPU9150_ACC_HPF_2_5_HZ              = 0x02,
//    MPU9150_ACC_HPF_1_25_HZ             = 0x03,
//    MPU9150_ACC_HPF_0_63_HZ             = 0x04,
//    MPU9150_ACC_HPF_HOLD                = 0x07
//} MPU9150_CONF_ACC_HPF;

typedef enum{
    MPU9150_TEMPSENSOR_ENABLE           = 0x00,     // Temp Sensor Enable
    MPU9150_TEMPSENSOR_DISABLE          = 0x80      // Temp Sensor Disable
} MPU9150_TEMPERATURE_SENSOR;

typedef enum {
    MPU9150_CLOCK_INTERNAL              = 0x00,     // Internal 8MHz oscillator
    MPU9150_CLOCK_PLL_XGYRO             = 0x01,     // PLL with X axis gyroscope reference
    MPU9150_CLOCK_PLL_YGYRO             = 0x02,     // PLL with Y axis gyroscope reference
    MPU9150_CLOCK_PLL_ZGYRO             = 0x03,     // PLL with Z axis gyroscope reference
    MPU9150_CLOCK_PLL_EXT32K            = 0x04,     // PLL with external 32.768kHz reference
    MPU9150_CLOCK_PLL_EXT19M            = 0x05,     // PLL with external 19.2MHz reference
    MPU9150_CLOCK_KEEP_RESET            = 0x07      // Stops the clock and keeps the timing generator in reset
} MPU9150_CLOCK_SOURCE;

typedef enum {
    MPU9150_MAG_FORWARD_RIGHT_DOWN      = 0x01,     // X->Forward, Y->Right, Z->Down
    MPU9150_MAG_FORWARD_LEFT_UP         = 0x02,     // X->Forward, Y->Left,  Z->Up
    MPU9150_ORIENT_TEST
} MPU9150_CONF_AXES_ORIENT;

typedef enum {
    MPU9150_


} MPU9150_CONF_SAMPLE_RATE;


//------------------------------------------------------------
// Config
//------------------------------------------------------------
typedef struct MPU9150_CONFIG{
    MPU9150_CONF_AD0_STATUS         AD0;        // AD00: LOW, HIGH, or AUTO

    MPU9150_CLOCK_SOURCE            ClkSel;

    MPU9150_CONF_ACC_FULLSCALE      AccFullScale;
    MPU9150_CONF_GYRO_FULLSCALE     GyroFullScale;
//    MPU9150_CONF_ACC_HPF            AccHPF;

    MPU9150_TEMPERATURE_SENSOR      TempSensor;

    MPU9150_CONF_AXES_ORIENT        AxesOrientation;
//    MPU9150_CONF_DLP_FILTER         DigitalLowPassFilter;
//    uint8                           SamplingRate;



} MPU9150_CONFIG;
//------------------------------------------------------------
// Public Data Structure /////////////////////////////////////
//------------------------------------------------------------
typedef struct EZOBJ_MPU9150 {
    void*       PrivateData;

    VECTOR3Df       a;  // accelerometer reading
    VECTOR3Df       g;  // gyroscope reading
    VECTOR3Df       m;  // magnetometer reading

    float           tempC;   //Temperature;

    MPU9150_CONFIG  Config;

} EZOBJ_MPU9150;

typedef EZOBJ_MPU9150 * PEZOBJ_MPU9150;


//------------------------------------------------------------
// Public Functions //////////////////////////////////////////
//------------------------------------------------------------
PEZOBJ_MPU9150     MPU9150_Create();
void                MPU9150_Release(PEZOBJ_MPU9150 mpu);

void                MPU9150_ConnectI2C(PEZOBJ_MPU9150 mpu, PEZOBJ_I2C i2c);

bool                MPU9150_Init (PEZOBJ_MPU9150 mpu);
void                MPU9150_Start(PEZOBJ_MPU9150 mpu);

void                MPU9150_SetAccOffset(PEZOBJ_MPU9150 mpu);
void                MPU9150_SetGyroOffset(PEZOBJ_MPU9150 mpu);
void                MPU9150_SetMagMax(PEZOBJ_MPU9150 mpu);
void                MPU9150_SetMagMin(PEZOBJ_MPU9150 mpu);
void                MPU9150_MeasureAccGyroOffcet(PEZOBJ_MPU9150 mpu);

bool                MPU9150_ReadAcc(PEZOBJ_MPU9150 mpu);           // Output Unit: mg
bool                MPU9150_ReadGyro(PEZOBJ_MPU9150 mpu);          // Output Uint: dps/sec
bool                MPU9150_ReadGyroInDeg(PEZOBJ_MPU9150 mpu);     // Output Uint: Deg/sec
bool                MPU9150_ReadGyroInRad(PEZOBJ_MPU9150 mpu);     // Output Uint: Rad/sec
bool                MPU9150_ReadMag(PEZOBJ_MPU9150 mpu);           // Output Unit: Gauss

bool                MPU9150_ReadTemp(PEZOBJ_MPU9150 mpu);

// Interface
void                MPU9150_GetAccIntf (PEZOBJ_MPU9150 mpu, PEZOBJ_INTERFACE intf);
void                MPU9150_GetMagIntf (PEZOBJ_MPU9150 mpu, PEZOBJ_INTERFACE intf);
void                MPU9150_GetGyroIntf(PEZOBJ_MPU9150 mpu, PEZOBJ_INTERFACE intf);

//------------------------------------------------------------
// Sample Code and Parameters ////////////////////////////////
//------------------------------------------------------------
/*
    mpu = MPU9150_Create();
    MPU9150_ConnectI2C(mpu, i2c);
    mpu->a.x =  45.0f;
    mpu->a.y =  23.0f;
    mpu->a.z =  -368.0f;
    MPU9150_SetAccOffset(mpu);
    mpu->g.x =  13.0f;
    mpu->g.y =  14.0f;
    mpu->g.z =  21.0f;
    MPU9150_SetGyroOffset(mpu);

    mpu->m.x =  161.0f;
    mpu->m.y =  257.0f;
    mpu->m.z =  -48.0f;
    MPU9150_SetMagMax(mpu);
    mpu->m.x = -116.0f;
    mpu->m.y = -35.0f;
    mpu->m.z = -322.0f;
    MPU9150_SetMagMin(mpu);

    MPU9150_Init(mpu);
    MPU9150_Start(mpu);
*/




#endif
//[] END OF FILE