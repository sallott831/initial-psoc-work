/* ========================================
 *
 * Copyright iAppSpace.com, 2013
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#if !defined(MATH4DATASAMPLING_H)
#define MATH4DATASAMPLING_H


struct sAve {
    int             blockSum;
    unsigned int    numSamples;
};

void  AddSampleToAverage(struct sAve *ave, int newSample);
unsigned int GetAverage(struct sAve *ave);

struct sVar {
    int             mean;
    unsigned int    M2;
    unsigned int    numSamples;
};

void            AddSampleToVariance( struct sVar *var, int newSample);
unsigned int    GetVariance( struct sVar *var, int *average);


#endif

//[] END OF FILE
