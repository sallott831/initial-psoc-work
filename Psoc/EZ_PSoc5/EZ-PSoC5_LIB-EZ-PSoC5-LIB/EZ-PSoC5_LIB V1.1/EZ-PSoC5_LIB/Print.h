/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#ifndef PRINT_H
#define PRINT_H

#include "MCP23008.h"
#include <cytypes.h>
#include "TypeDefine.h"

#include <stdio.h> // for size_t 

#define DEC     10
#define HEX     16
#define OCT     8
#define BIN     2
#define BYTE    0


typedef struct EZOBJ_PRINT
{


} EZOBJ_PRINT;

typedef EZOBJ_PRINT * PEZOBJ_PRINT;



PEZOBJ_PRINT       Print_Create();
void                Print_Release(PEZOBJ_PRINT prt);

bool                Print_Init (PEZOBJ_PRINT *I2C);
void                Print_SetupDevice(PEZOBJ_MCP23008 mcp);









#endif
//[] END OF FILE