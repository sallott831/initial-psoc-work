/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/


#if !defined(VECTOR_N_MATRIX_H)
#define VECTOR_N_MATRIX_H

void VECTOR_Add(float vOut[],float v1[], float v2[]);

//Multiply the vector by a scalar.
void VECTOR_Scale(float vOut[],float v[], float scale);

//Computes the cross product of two vectors
void VECTOR_CrossProduct(float vOut[], float v1[],float v2[]);

//Computes the dot product of two vectors
float VECTOR_DotProduct(float v1[],float v2[]);

// mat[] = a[] * b[]
void MATRIX_Multiply_3X3(float vOut[][3], float v1[][3], float v2[][3]);






#endif
//[] END OF FILE