/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#ifndef ezISR_H
#define ezISR_H

//typedef struct EZOBJ_ISR
//{
//    void*           PrivateData;
//
//    void            (* ISR_Callback)(void);
//} EZOBJ_ISR;
//
//typedef EZOBJ_ISR * PEZOBJ_ISR;
//

#define ezISR(X)    void X##_ezISRVector() \
{                             \
    X##->ISR_Callback();      \
}




#define CONNECT_ISR(X,Y)    Y##_StartEx(X##_ezISRVector);

//
//PEZOBJ_ISR         ezISR_Create();
//void                ezISR_Release(PEZOBJ_ISR isr);







#endif
//[] END OF FILE