/*******************************************************************************
* File Name: main.c
*
* Version: 1.0
*
* Description:
*  This is a code example for the EZI2C Slave and I2C Master (UDB) Components.
*  It demonstrates usage of EZI2C Slave and I2C Master Components. The
*  Components are connected off-chip. Transfer of data in both directions (read
*  EZI2C and write EZI2C) is demonstrated. A Character LCD Component is included
*  to display the contents of both data buffers.
*
********************************************************************************
* Copyright 2014, Cypress Semiconductor Corporation. All rights reserved.
* This software is owned by Cypress Semiconductor Corporation and is protected
* by and subject to worldwide patent and copyright laws and treaties.
* Therefore, you may use this software only as provided in the license agreement
* accompanying the software package from which you obtained this software.
* CYPRESS AND ITS SUPPLIERS MAKE NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
* WITH REGARD TO THIS SOFTWARE, INCLUDING, BUT NOT LIMITED TO, NONINFRINGEMENT,
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
*******************************************************************************/

#include <device.h>

/* Function Prototypes */
uint8 I2C_MasterWriteToEZI2C(uint8 i2CAddr, uint8 nbytes);

/* Constants */
#define EZI2C_SLAVE_ADDR (8u)
#define EZI2C_SIZE       (4u)
/* Sets the boundary between the read/write and read only areas.
*  The read/write area is first, followed by the read only area. */
#define EZI2C_RW_SIZE    (2u)
/* button bit assignments in Buttons status register */
#define SW2  (1u)
#define SW3  (2u)

/* Data Structure Definitions */
/* I2C master data, packed for 32-bit CPU */
#if CY_PSOC3
#define MASTER_BUF /* PSoC 3 */
#else
#define MASTER_BUF __attribute__ ((packed)) /* PSoC 5 */
#endif
typedef struct
{
    uint8 writeOffset; /* 1 byte because EZI2C Sub-address is 8 bits */
    uint8 i2cMasterBuffer[EZI2C_SIZE];
} MASTER_BUF EZI2C_DATASTRUCT;

/* Variables */
/* EZI2C buffer */
uint8 ezi2cBuffer[EZI2C_SIZE] = {0, 0, 0, 0};
/* master buffer */
EZI2C_DATASTRUCT i2cMasterData = {0, {0, 0, 0, 0}};


/*******************************************************************************
* Function Name: main
********************************************************************************
*
* Summary:
*  Main function performs following functions:
*   1. Initializes all Components as well as global interrupts.
*   2. Initializes EZI2C and Master data buffers.
*   3. Reads buttons status
*   3a. If SW2 is pressed, EZI2C side updates its data buffer.
*   3b. If SW3 is pressed, I2C master side does the following:
*       - Reads the EZI2C buffer to its data buffer.
*       - Updates the write portion of its data buffer.
*       - Writes the write portion of its data buffer to the EZI2C.
*   4. Displays the contents of both the EZI2C and the I2C master data buffers.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
int main(void)
{
    uint8 buttons;         /* copy of Buttons status reg */
    uint8 dataValue;       /* temporary variable to highlight read and write operations */
    uint8 i;               /* general purpose counter */
    uint8 volatile status; /* copy of EZI2C activity status */

    /* Initialize Components */
    LCD_Start();

    /* Enable global interrupts -
    *  the EZI2C and I2C_Master Components use interrupts */
    CyGlobalIntEnable;

    /* initialize EZI2C and its buffer */
    EZI2C_Start();
    /* EZI2C buffer parameters:
    *  EZI2C_SIZE - is the size of the memory exposed to the I2C interface.
    *  EZI2C_RW_SIZE - sets the boundary between the read/write and read only areas.
    *                  The read/write area is first, followed by the read only area.
    *  (void *)ezi2cBuffer - is the pointer to the memory exposed to the I2C interface. */
    EZI2C_SetBuffer1(EZI2C_SIZE, EZI2C_RW_SIZE, (void *)ezi2cBuffer);

    /* initialize I2C master */
    I2CM_Start();

    /* main loop */
    for(;;)
    {
        /* Read SW2 and SW3 buttons.
        *  Reading clears the sticky bits in the Status Register. */
        buttons = Buttons_Read();

        /*************************************************************************************/
        /* Do slave side tasks, with the EZI2C Component                                     */
        /*************************************************************************************/
        /* SW2: I2C Slave updates EZI2C data */
        if(buttons & SW2)
        {
            /* Basic Access - Increment all bytes in the EZI2C buffer, by different values */
            for(i = 0u; i < EZI2C_SIZE; i++)
            {
                /* ezi2c buffer read */
                dataValue = ezi2cBuffer[i];
                /* ezi2c buffer write */
                ezi2cBuffer[i] = dataValue + i + 1;
            }
        }
        
        /*************************************************************************************/
        /* Slave side optional features of the EZI2C Component                               */
        /*************************************************************************************/
		/* Optional - The EZI2C activity status can be read */
        status = EZI2C_GetActivity();
        
        /* Optional - Check for EZI2C hardware error */
        if(status & EZI2C_STATUS_ERR)
        {
            /* error handler goes here*/
        }
        
        /* Optional - Synchronize slave reads and writes of data buffer with master 
        *  access to provide safe atomic data operations. 
        *  EZI2C_GetActivity returns 0 if no master activity or errors. */
        if(!status)
        {
            /* Disable the EZI2C component interrupt, locking the bus. */
            EZI2C_DisableInt();
            /* Make sure master didn't start access since last check */
            status = EZI2C_GetActivity();
            if(!status)
            {
                /* Perform reads or writes to ezi2cBuffer[] here */
            }
            /* Enable the EZI2C component interrupt, unlocking the bus. */
            EZI2C_EnableInt();
        }
            
        /* Optional - Perform action after Master accesses buffer */
        /* Display master data buffer after master write to slave.
        *  Char LCD display legend:
        *  Bottom row: data in I2C master data buffer, LSbyte first "MST: xx xx xx xx" */
        if((status & (EZI2C_STATUS_WRITE1 | EZI2C_STATUS_BUSY)) == EZI2C_STATUS_WRITE1)
        {
            LCD_Position(1u, 0u);     /* (row, column) */
            LCD_PrintString("MST: "); /* I2C Master */
            for(i = 0u; i < EZI2C_SIZE; i++)
            {
                LCD_PrintInt8(i2cMasterData.i2cMasterBuffer[i]);
                LCD_PrintString(" ");
            }
        }

        /*************************************************************************************/
        /* Do master side tasks, with the I2C Master Component, with error handling          */
        /*************************************************************************************/
        /* SW3: I2C Master updates EZI2C data (RW portion only) */
        if(buttons & SW3)
        {
            /* Read the data from the EZI2C */
            /* First, write the offset byte. This is the first byte in the global
            *  structure i2cMasterData. That is, i2cMasterData.writeOffset. */
            status = I2C_MasterWriteToEZI2C(EZI2C_SLAVE_ADDR, 1);
            if(!(status & I2CM_MSTAT_ERR_XFER))
            {
                /* Then do the read */
                status = I2CM_MasterClearStatus();
                if(!(status & I2CM_MSTAT_ERR_XFER))
                {
                    status = I2CM_MasterReadBuf(EZI2C_SLAVE_ADDR,
                                               (uint8 *)&(i2cMasterData.i2cMasterBuffer),
                                               EZI2C_SIZE, I2CM_MODE_COMPLETE_XFER);
                    if(status == I2CM_MSTR_NO_ERROR)
                    {
                        /* wait for read complete and no error */
                        do
                        {
                            status = I2CM_MasterStatus();
                        } while((status & (I2CM_MSTAT_RD_CMPLT | I2CM_MSTAT_ERR_XFER)) == 0u);
                        if(!(status & I2CM_MSTAT_ERR_XFER))
                        {
                            /* Decrement all RW bytes in the EZI2C buffer, by different values */
                            for(i = 0u; i < EZI2C_RW_SIZE; i++)
                            {
                                i2cMasterData.i2cMasterBuffer[i] -= (i + 1);
                            }
                            /* Write data to the EZI2C (RW portion only).
                            *  When writing to EZI2C, write the offset byte followed by the write data. */
                            status = I2C_MasterWriteToEZI2C(EZI2C_SLAVE_ADDR, 1 + EZI2C_RW_SIZE);
                        }
                    }
                    else
                    {
                        /* translate from I2CM_MasterReadBuf() error output to
                        *  I2CM_MasterStatus() error output */
                        status = I2CM_MSTAT_ERR_XFER;
                    }
                }
            }
            if(status & I2CM_MSTAT_ERR_XFER)
            {
                /* add error handler code here */
            }
        }

        /* Display EZI2C buffer   
        *  Char LCD display legend:
        *  Top row: data in EZI2C data buffer, LSbyte first  "EZ:  xx xx xx xx" */
        LCD_Position(0u, 0u);     /* row, column */
        LCD_PrintString("EZ:  "); /* EZI2C */
        for(i = 0u; i < EZI2C_SIZE; i++)
        {
            LCD_PrintInt8(ezi2cBuffer[i]);
            LCD_PrintString(" ");
        }
        
    } /* end of main loop */
} /* end of main() */


/*******************************************************************************
* Function Name: I2CMasterWriteToEZI2C
********************************************************************************
*
* Summary:
*  Writes from the I2C master (global struct i2cMasterData) to the EZI2C buffer.
*  This function is blocking: it writes with mode I2CM_MODE_COMPLETE_XFER, and
*  waits until I2CM_MSTAT_RD_CMPLT is set.
*
* Parameters:
*  1. i2cAddr: EZI2C slave I2C address
*  2. nbytes: # of bytes to write
*
* Return:
*  Status / error: see I2C_MasterStatus() return values in datasheet
*
*******************************************************************************/
uint8 I2C_MasterWriteToEZI2C(uint8 i2CAddr, uint8 nbytes)
{
    uint8 volatile status;
    
    status = I2CM_MasterClearStatus();
    if(!(status & I2CM_MSTAT_ERR_XFER))
    {
        status = I2CM_MasterWriteBuf(i2CAddr, (uint8 *)&i2cMasterData, nbytes,
                                     I2CM_MODE_COMPLETE_XFER);
        if(status == I2CM_MSTR_NO_ERROR)
        {
            /* wait for write complete and no error */
            do
            {
                status = I2CM_MasterStatus();
            } while((status & (I2CM_MSTAT_WR_CMPLT | I2CM_MSTAT_ERR_XFER)) == 0u);
        }
        else
        {
            /* translate from I2CM_MasterWriteBuf() error output to
            *  I2CM_MasterStatus() error output */
            status = I2CM_MSTAT_ERR_XFER;
        }
    }
    
    return status;
}

/* [] END OF FILE */
