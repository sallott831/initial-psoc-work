/*******************************************************************************
* File Name: BluLED.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_BluLED_H) /* Pins BluLED_H */
#define CY_PINS_BluLED_H

#include "cytypes.h"
#include "cyfitter.h"
#include "BluLED_aliases.h"


/***************************************
*     Data Struct Definitions
***************************************/

/**
* \addtogroup group_structures
* @{
*/
    
/* Structure for sleep mode support */
typedef struct
{
    uint32 pcState; /**< State of the port control register */
    uint32 sioState; /**< State of the SIO configuration */
    uint32 usbState; /**< State of the USBIO regulator */
} BluLED_BACKUP_STRUCT;

/** @} structures */


/***************************************
*        Function Prototypes             
***************************************/
/**
* \addtogroup group_general
* @{
*/
uint8   BluLED_Read(void);
void    BluLED_Write(uint8 value);
uint8   BluLED_ReadDataReg(void);
#if defined(BluLED__PC) || (CY_PSOC4_4200L) 
    void    BluLED_SetDriveMode(uint8 mode);
#endif
void    BluLED_SetInterruptMode(uint16 position, uint16 mode);
uint8   BluLED_ClearInterrupt(void);
/** @} general */

/**
* \addtogroup group_power
* @{
*/
void BluLED_Sleep(void); 
void BluLED_Wakeup(void);
/** @} power */


/***************************************
*           API Constants        
***************************************/
#if defined(BluLED__PC) || (CY_PSOC4_4200L) 
    /* Drive Modes */
    #define BluLED_DRIVE_MODE_BITS        (3)
    #define BluLED_DRIVE_MODE_IND_MASK    (0xFFFFFFFFu >> (32 - BluLED_DRIVE_MODE_BITS))

    /**
    * \addtogroup group_constants
    * @{
    */
        /** \addtogroup driveMode Drive mode constants
         * \brief Constants to be passed as "mode" parameter in the BluLED_SetDriveMode() function.
         *  @{
         */
        #define BluLED_DM_ALG_HIZ         (0x00u) /**< \brief High Impedance Analog   */
        #define BluLED_DM_DIG_HIZ         (0x01u) /**< \brief High Impedance Digital  */
        #define BluLED_DM_RES_UP          (0x02u) /**< \brief Resistive Pull Up       */
        #define BluLED_DM_RES_DWN         (0x03u) /**< \brief Resistive Pull Down     */
        #define BluLED_DM_OD_LO           (0x04u) /**< \brief Open Drain, Drives Low  */
        #define BluLED_DM_OD_HI           (0x05u) /**< \brief Open Drain, Drives High */
        #define BluLED_DM_STRONG          (0x06u) /**< \brief Strong Drive            */
        #define BluLED_DM_RES_UPDWN       (0x07u) /**< \brief Resistive Pull Up/Down  */
        /** @} driveMode */
    /** @} group_constants */
#endif

/* Digital Port Constants */
#define BluLED_MASK               BluLED__MASK
#define BluLED_SHIFT              BluLED__SHIFT
#define BluLED_WIDTH              1u

/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in BluLED_SetInterruptMode() function.
     *  @{
     */
        #define BluLED_INTR_NONE      ((uint16)(0x0000u)) /**< \brief Disabled             */
        #define BluLED_INTR_RISING    ((uint16)(0x5555u)) /**< \brief Rising edge trigger  */
        #define BluLED_INTR_FALLING   ((uint16)(0xaaaau)) /**< \brief Falling edge trigger */
        #define BluLED_INTR_BOTH      ((uint16)(0xffffu)) /**< \brief Both edge trigger    */
    /** @} intrMode */
/** @} group_constants */

/* SIO LPM definition */
#if defined(BluLED__SIO)
    #define BluLED_SIO_LPM_MASK       (0x03u)
#endif

/* USBIO definitions */
#if !defined(BluLED__PC) && (CY_PSOC4_4200L)
    #define BluLED_USBIO_ENABLE               ((uint32)0x80000000u)
    #define BluLED_USBIO_DISABLE              ((uint32)(~BluLED_USBIO_ENABLE))
    #define BluLED_USBIO_SUSPEND_SHIFT        CYFLD_USBDEVv2_USB_SUSPEND__OFFSET
    #define BluLED_USBIO_SUSPEND_DEL_SHIFT    CYFLD_USBDEVv2_USB_SUSPEND_DEL__OFFSET
    #define BluLED_USBIO_ENTER_SLEEP          ((uint32)((1u << BluLED_USBIO_SUSPEND_SHIFT) \
                                                        | (1u << BluLED_USBIO_SUSPEND_DEL_SHIFT)))
    #define BluLED_USBIO_EXIT_SLEEP_PH1       ((uint32)~((uint32)(1u << BluLED_USBIO_SUSPEND_SHIFT)))
    #define BluLED_USBIO_EXIT_SLEEP_PH2       ((uint32)~((uint32)(1u << BluLED_USBIO_SUSPEND_DEL_SHIFT)))
    #define BluLED_USBIO_CR1_OFF              ((uint32)0xfffffffeu)
#endif


/***************************************
*             Registers        
***************************************/
/* Main Port Registers */
#if defined(BluLED__PC)
    /* Port Configuration */
    #define BluLED_PC                 (* (reg32 *) BluLED__PC)
#endif
/* Pin State */
#define BluLED_PS                     (* (reg32 *) BluLED__PS)
/* Data Register */
#define BluLED_DR                     (* (reg32 *) BluLED__DR)
/* Input Buffer Disable Override */
#define BluLED_INP_DIS                (* (reg32 *) BluLED__PC2)

/* Interrupt configuration Registers */
#define BluLED_INTCFG                 (* (reg32 *) BluLED__INTCFG)
#define BluLED_INTSTAT                (* (reg32 *) BluLED__INTSTAT)

/* "Interrupt cause" register for Combined Port Interrupt (AllPortInt) in GSRef component */
#if defined (CYREG_GPIO_INTR_CAUSE)
    #define BluLED_INTR_CAUSE         (* (reg32 *) CYREG_GPIO_INTR_CAUSE)
#endif

/* SIO register */
#if defined(BluLED__SIO)
    #define BluLED_SIO_REG            (* (reg32 *) BluLED__SIO)
#endif /* (BluLED__SIO_CFG) */

/* USBIO registers */
#if !defined(BluLED__PC) && (CY_PSOC4_4200L)
    #define BluLED_USB_POWER_REG       (* (reg32 *) CYREG_USBDEVv2_USB_POWER_CTRL)
    #define BluLED_CR1_REG             (* (reg32 *) CYREG_USBDEVv2_CR1)
    #define BluLED_USBIO_CTRL_REG      (* (reg32 *) CYREG_USBDEVv2_USB_USBIO_CTRL)
#endif    
    
    
/***************************************
* The following code is DEPRECATED and 
* must not be used in new designs.
***************************************/
/**
* \addtogroup group_deprecated
* @{
*/
#define BluLED_DRIVE_MODE_SHIFT       (0x00u)
#define BluLED_DRIVE_MODE_MASK        (0x07u << BluLED_DRIVE_MODE_SHIFT)
/** @} deprecated */

#endif /* End Pins BluLED_H */


/* [] END OF FILE */
