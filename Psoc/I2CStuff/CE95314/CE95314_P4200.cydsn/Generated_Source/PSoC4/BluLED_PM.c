/*******************************************************************************
* File Name: BluLED.c  
* Version 2.20
*
* Description:
*  This file contains APIs to set up the Pins component for low power modes.
*
* Note:
*
********************************************************************************
* Copyright 2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "cytypes.h"
#include "BluLED.h"

static BluLED_BACKUP_STRUCT  BluLED_backup = {0u, 0u, 0u};


/*******************************************************************************
* Function Name: BluLED_Sleep
****************************************************************************//**
*
* \brief Stores the pin configuration and prepares the pin for entering chip 
*  deep-sleep/hibernate modes. This function must be called for SIO and USBIO
*  pins. It is not essential if using GPIO or GPIO_OVT pins.
*
* <b>Note</b> This function is available in PSoC 4 only.
*
* \return 
*  None 
*  
* \sideeffect
*  For SIO pins, this function configures the pin input threshold to CMOS and
*  drive level to Vddio. This is needed for SIO pins when in device 
*  deep-sleep/hibernate modes.
*
* \funcusage
*  \snippet BluLED_SUT.c usage_BluLED_Sleep_Wakeup
*******************************************************************************/
void BluLED_Sleep(void)
{
    #if defined(BluLED__PC)
        BluLED_backup.pcState = BluLED_PC;
    #else
        #if (CY_PSOC4_4200L)
            /* Save the regulator state and put the PHY into suspend mode */
            BluLED_backup.usbState = BluLED_CR1_REG;
            BluLED_USB_POWER_REG |= BluLED_USBIO_ENTER_SLEEP;
            BluLED_CR1_REG &= BluLED_USBIO_CR1_OFF;
        #endif
    #endif
    #if defined(CYIPBLOCK_m0s8ioss_VERSION) && defined(BluLED__SIO)
        BluLED_backup.sioState = BluLED_SIO_REG;
        /* SIO requires unregulated output buffer and single ended input buffer */
        BluLED_SIO_REG &= (uint32)(~BluLED_SIO_LPM_MASK);
    #endif  
}


/*******************************************************************************
* Function Name: BluLED_Wakeup
****************************************************************************//**
*
* \brief Restores the pin configuration that was saved during Pin_Sleep().
*
* For USBIO pins, the wakeup is only triggered for falling edge interrupts.
*
* <b>Note</b> This function is available in PSoC 4 only.
*
* \return 
*  None
*  
* \funcusage
*  Refer to BluLED_Sleep() for an example usage.
*******************************************************************************/
void BluLED_Wakeup(void)
{
    #if defined(BluLED__PC)
        BluLED_PC = BluLED_backup.pcState;
    #else
        #if (CY_PSOC4_4200L)
            /* Restore the regulator state and come out of suspend mode */
            BluLED_USB_POWER_REG &= BluLED_USBIO_EXIT_SLEEP_PH1;
            BluLED_CR1_REG = BluLED_backup.usbState;
            BluLED_USB_POWER_REG &= BluLED_USBIO_EXIT_SLEEP_PH2;
        #endif
    #endif
    #if defined(CYIPBLOCK_m0s8ioss_VERSION) && defined(BluLED__SIO)
        BluLED_SIO_REG = BluLED_backup.sioState;
    #endif
}


/* [] END OF FILE */
