/*******************************************************************************
* File Name: BluLED.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_BluLED_ALIASES_H) /* Pins BluLED_ALIASES_H */
#define CY_PINS_BluLED_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"


/***************************************
*              Constants        
***************************************/
#define BluLED_0			(BluLED__0__PC)
#define BluLED_0_PS		(BluLED__0__PS)
#define BluLED_0_PC		(BluLED__0__PC)
#define BluLED_0_DR		(BluLED__0__DR)
#define BluLED_0_SHIFT	(BluLED__0__SHIFT)
#define BluLED_0_INTR	((uint16)((uint16)0x0003u << (BluLED__0__SHIFT*2u)))

#define BluLED_INTR_ALL	 ((uint16)(BluLED_0_INTR))


#endif /* End Pins BluLED_ALIASES_H */


/* [] END OF FILE */
