/*******************************************************************************
* File Name: GrnLED.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_GrnLED_H) /* Pins GrnLED_H */
#define CY_PINS_GrnLED_H

#include "cytypes.h"
#include "cyfitter.h"
#include "GrnLED_aliases.h"


/***************************************
*     Data Struct Definitions
***************************************/

/**
* \addtogroup group_structures
* @{
*/
    
/* Structure for sleep mode support */
typedef struct
{
    uint32 pcState; /**< State of the port control register */
    uint32 sioState; /**< State of the SIO configuration */
    uint32 usbState; /**< State of the USBIO regulator */
} GrnLED_BACKUP_STRUCT;

/** @} structures */


/***************************************
*        Function Prototypes             
***************************************/
/**
* \addtogroup group_general
* @{
*/
uint8   GrnLED_Read(void);
void    GrnLED_Write(uint8 value);
uint8   GrnLED_ReadDataReg(void);
#if defined(GrnLED__PC) || (CY_PSOC4_4200L) 
    void    GrnLED_SetDriveMode(uint8 mode);
#endif
void    GrnLED_SetInterruptMode(uint16 position, uint16 mode);
uint8   GrnLED_ClearInterrupt(void);
/** @} general */

/**
* \addtogroup group_power
* @{
*/
void GrnLED_Sleep(void); 
void GrnLED_Wakeup(void);
/** @} power */


/***************************************
*           API Constants        
***************************************/
#if defined(GrnLED__PC) || (CY_PSOC4_4200L) 
    /* Drive Modes */
    #define GrnLED_DRIVE_MODE_BITS        (3)
    #define GrnLED_DRIVE_MODE_IND_MASK    (0xFFFFFFFFu >> (32 - GrnLED_DRIVE_MODE_BITS))

    /**
    * \addtogroup group_constants
    * @{
    */
        /** \addtogroup driveMode Drive mode constants
         * \brief Constants to be passed as "mode" parameter in the GrnLED_SetDriveMode() function.
         *  @{
         */
        #define GrnLED_DM_ALG_HIZ         (0x00u) /**< \brief High Impedance Analog   */
        #define GrnLED_DM_DIG_HIZ         (0x01u) /**< \brief High Impedance Digital  */
        #define GrnLED_DM_RES_UP          (0x02u) /**< \brief Resistive Pull Up       */
        #define GrnLED_DM_RES_DWN         (0x03u) /**< \brief Resistive Pull Down     */
        #define GrnLED_DM_OD_LO           (0x04u) /**< \brief Open Drain, Drives Low  */
        #define GrnLED_DM_OD_HI           (0x05u) /**< \brief Open Drain, Drives High */
        #define GrnLED_DM_STRONG          (0x06u) /**< \brief Strong Drive            */
        #define GrnLED_DM_RES_UPDWN       (0x07u) /**< \brief Resistive Pull Up/Down  */
        /** @} driveMode */
    /** @} group_constants */
#endif

/* Digital Port Constants */
#define GrnLED_MASK               GrnLED__MASK
#define GrnLED_SHIFT              GrnLED__SHIFT
#define GrnLED_WIDTH              1u

/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in GrnLED_SetInterruptMode() function.
     *  @{
     */
        #define GrnLED_INTR_NONE      ((uint16)(0x0000u)) /**< \brief Disabled             */
        #define GrnLED_INTR_RISING    ((uint16)(0x5555u)) /**< \brief Rising edge trigger  */
        #define GrnLED_INTR_FALLING   ((uint16)(0xaaaau)) /**< \brief Falling edge trigger */
        #define GrnLED_INTR_BOTH      ((uint16)(0xffffu)) /**< \brief Both edge trigger    */
    /** @} intrMode */
/** @} group_constants */

/* SIO LPM definition */
#if defined(GrnLED__SIO)
    #define GrnLED_SIO_LPM_MASK       (0x03u)
#endif

/* USBIO definitions */
#if !defined(GrnLED__PC) && (CY_PSOC4_4200L)
    #define GrnLED_USBIO_ENABLE               ((uint32)0x80000000u)
    #define GrnLED_USBIO_DISABLE              ((uint32)(~GrnLED_USBIO_ENABLE))
    #define GrnLED_USBIO_SUSPEND_SHIFT        CYFLD_USBDEVv2_USB_SUSPEND__OFFSET
    #define GrnLED_USBIO_SUSPEND_DEL_SHIFT    CYFLD_USBDEVv2_USB_SUSPEND_DEL__OFFSET
    #define GrnLED_USBIO_ENTER_SLEEP          ((uint32)((1u << GrnLED_USBIO_SUSPEND_SHIFT) \
                                                        | (1u << GrnLED_USBIO_SUSPEND_DEL_SHIFT)))
    #define GrnLED_USBIO_EXIT_SLEEP_PH1       ((uint32)~((uint32)(1u << GrnLED_USBIO_SUSPEND_SHIFT)))
    #define GrnLED_USBIO_EXIT_SLEEP_PH2       ((uint32)~((uint32)(1u << GrnLED_USBIO_SUSPEND_DEL_SHIFT)))
    #define GrnLED_USBIO_CR1_OFF              ((uint32)0xfffffffeu)
#endif


/***************************************
*             Registers        
***************************************/
/* Main Port Registers */
#if defined(GrnLED__PC)
    /* Port Configuration */
    #define GrnLED_PC                 (* (reg32 *) GrnLED__PC)
#endif
/* Pin State */
#define GrnLED_PS                     (* (reg32 *) GrnLED__PS)
/* Data Register */
#define GrnLED_DR                     (* (reg32 *) GrnLED__DR)
/* Input Buffer Disable Override */
#define GrnLED_INP_DIS                (* (reg32 *) GrnLED__PC2)

/* Interrupt configuration Registers */
#define GrnLED_INTCFG                 (* (reg32 *) GrnLED__INTCFG)
#define GrnLED_INTSTAT                (* (reg32 *) GrnLED__INTSTAT)

/* "Interrupt cause" register for Combined Port Interrupt (AllPortInt) in GSRef component */
#if defined (CYREG_GPIO_INTR_CAUSE)
    #define GrnLED_INTR_CAUSE         (* (reg32 *) CYREG_GPIO_INTR_CAUSE)
#endif

/* SIO register */
#if defined(GrnLED__SIO)
    #define GrnLED_SIO_REG            (* (reg32 *) GrnLED__SIO)
#endif /* (GrnLED__SIO_CFG) */

/* USBIO registers */
#if !defined(GrnLED__PC) && (CY_PSOC4_4200L)
    #define GrnLED_USB_POWER_REG       (* (reg32 *) CYREG_USBDEVv2_USB_POWER_CTRL)
    #define GrnLED_CR1_REG             (* (reg32 *) CYREG_USBDEVv2_CR1)
    #define GrnLED_USBIO_CTRL_REG      (* (reg32 *) CYREG_USBDEVv2_USB_USBIO_CTRL)
#endif    
    
    
/***************************************
* The following code is DEPRECATED and 
* must not be used in new designs.
***************************************/
/**
* \addtogroup group_deprecated
* @{
*/
#define GrnLED_DRIVE_MODE_SHIFT       (0x00u)
#define GrnLED_DRIVE_MODE_MASK        (0x07u << GrnLED_DRIVE_MODE_SHIFT)
/** @} deprecated */

#endif /* End Pins GrnLED_H */


/* [] END OF FILE */
