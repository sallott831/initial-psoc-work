/*******************************************************************************
* File Name: GrnLED.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_GrnLED_ALIASES_H) /* Pins GrnLED_ALIASES_H */
#define CY_PINS_GrnLED_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"


/***************************************
*              Constants        
***************************************/
#define GrnLED_0			(GrnLED__0__PC)
#define GrnLED_0_PS		(GrnLED__0__PS)
#define GrnLED_0_PC		(GrnLED__0__PC)
#define GrnLED_0_DR		(GrnLED__0__DR)
#define GrnLED_0_SHIFT	(GrnLED__0__SHIFT)
#define GrnLED_0_INTR	((uint16)((uint16)0x0003u << (GrnLED__0__SHIFT*2u)))

#define GrnLED_INTR_ALL	 ((uint16)(GrnLED_0_INTR))


#endif /* End Pins GrnLED_ALIASES_H */


/* [] END OF FILE */
