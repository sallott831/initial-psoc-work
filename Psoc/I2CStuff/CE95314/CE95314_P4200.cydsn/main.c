/*******************************************************************************
* File Name: main.c
*
* Version: 1.0
*
* Description:
*  This is a code example for the EZI2C Slave (SCB mode) and I2C (SCB mode)
*  Components. The I2C (SCB mode) is configured as an I2C master. The Components
*  are connected off-chip. Transfer of data in both directions (read EZI2C and
*  write EZI2C) is demonstrated. The RGB LEDs on CY8CKIT-042 are used to display
*  the contents of the data buffer.
*
********************************************************************************
* Copyright 2014, Cypress Semiconductor Corporation. All rights reserved.
* This software is owned by Cypress Semiconductor Corporation and is protected
* by and subject to worldwide patent and copyright laws and treaties.
* Therefore, you may use this software only as provided in the license agreement
* accompanying the software package from which you obtained this software.
* CYPRESS AND ITS SUPPLIERS MAKE NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
* WITH REGARD TO THIS SOFTWARE, INCLUDING, BUT NOT LIMITED TO, NONINFRINGEMENT,
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
*******************************************************************************/

#include <device.h>

/* Function Prototypes */
uint8 I2C_MasterWriteToEZI2C(uint8 i2CAddr, uint8 nbytes);

/* Constants */
#define EZI2C_SLAVE_ADDR (8u)
#define EZI2C_SIZE       (sizeof(ezi2cBuffer))
/* Sets the boundary between the read/write and read only areas.
*  The read/write area is first, followed by the read only area.
*  In this case, the read/write area size is the size of
*  the structure member 'reload'.
*/
#define EZI2C_RW_SIZE     (sizeof(ezi2cBuffer.reload))
/* button bit assignment */
#define SW2  (1u)
/* for rate of changing LEDs, at 24 MHz CPU clock */
#define RELOAD_INIT  (100u)
#define RELOAD_MAX   (0x8000u)

/* Data Structure Definitions */
/* Common data structure used by both EZI2C and I2C Master */
typedef struct
{
    uint16 reload; /* EZI2C counter reload value: read/write */
    uint8  leds;   /* LEDs control bits: read only */
} __attribute__ ((packed)) EZI2C_DATASTRUCT;

/* Variables */
/* EZI2C buffer */
EZI2C_DATASTRUCT ezi2cBuffer = {RELOAD_INIT, 0};
/* I2C master buffer */
struct
{
    uint8 writeOffset; /* 1 byte because EZI2C Sub-address is 8 bits */
    EZI2C_DATASTRUCT i2cMasterBuffer;
} __attribute__ ((packed)) i2cMasterData = {0, {0, 0}};


/*******************************************************************************
* Function Name: main
********************************************************************************
*
* Summary:
*  Main function performs following functions:
*   1. Initializes all Components as well as global interrupts.
*   2. Initializes EZI2C and Master data buffers.
*   3. EZI2C side changes LED control bits in its buffer, using a CPU-based
*      counter with a reload value in its buffer. (See Control Reg in schematic
*      for bit settings.)
*   4. I2C master side reads the EZI2C buffer to its data buffer.
*   5. If SW2 is pressed, I2C master side does the following:
*      - Updates the write portion of its data buffer, to change the rate at
*        which the LED color is changed.
*      - Writes the write portion of its data buffer to the EZI2C.
*   6. I2C master side updates LEDs per control byte in its buffer.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
int main(void)
{
    uint16 counter = 0;    /* for EZI2C changing LEDs */
    uint8  buttons = 0;    /* copy of Buttons status reg */
    uint8 volatile status; /* copy of EZI2C activity status */

    /* Enable global interrupts -
    *  the EZI2C and I2C_Master Components use interrupts
    */
    CyGlobalIntEnable;

    /* initialize EZI2C and its buffer */
    EZI2C_Start();
    /* EZI2C buffer parameters:
    *  EZI2C_SIZE - is the size of the memory exposed to the I2C interface.
    *  EZI2C_RW_SIZE - sets the boundary between the read/write and read only areas.
    *                  The read/write area is first, followed by the read only area.
    *  (void *)ezi2cBuffer - is the pointer to the memory exposed to the I2C interface. */
    EZI2C_EzI2CSetBuffer1(EZI2C_SIZE, EZI2C_RW_SIZE, (void *)&ezi2cBuffer);

    /* initialize I2C master */
    I2CM_Start();

    /* main loop */
    for(;;)
    {
        /* Read SW2 button.
        *  Reading clears the sticky bits in the Status Register. */
        buttons = Buttons_Read();

        /*************************************************************************************/
        /* Do slave side tasks, with the EZI2C Component, with options and error handling    */
        /*************************************************************************************/
        /* I2C Slave changes LEDs at a counter-determined rate */
        if(counter == 0)
        {
            counter = ezi2cBuffer.reload;
            /* cycle LEDs value through 0, 1, 2, 4 */
            if(ezi2cBuffer.leds == 0)
            {
                ezi2cBuffer.leds = 1;
            }
            else
            {
                if((ezi2cBuffer.leds <<= 1) > 4)
                {
                    ezi2cBuffer.leds = 0;
                }
            }
        }
        else
        {
            counter--;
        }

        /*************************************************************************************/
        /* Slave side optional features of the EZI2C Component                               */
        /*************************************************************************************/
        /* Optional - The EZI2C activity status can be read */
        status = EZI2C_EzI2CGetActivity();
        
        /* Optional - Check for EZI2C hardware error */
        if(status & EZI2C_EZI2C_STATUS_ERR)
        {
            /* error handler goes here*/
        }

        /*************************************************************************************/
        /* Do master side tasks, with the I2C Master Component, with error handling          */
        /*************************************************************************************/
        /* Read the data from the EZI2C */
        /* First, write the offset byte. This is the first byte in the global
        *  structure i2cMasterData. That is, i2cMasterData.writeOffset. */
        status = I2C_MasterWriteToEZI2C(EZI2C_SLAVE_ADDR, 1);
        if(!(status & I2CM_I2C_MSTAT_ERR_XFER))
        {
            /* Then do the read */
            status = I2CM_I2CMasterClearStatus();
            if(!(status & I2CM_I2C_MSTAT_ERR_XFER))
            {
                status = I2CM_I2CMasterReadBuf(EZI2C_SLAVE_ADDR,
                                               (uint8 *)&(i2cMasterData.i2cMasterBuffer),
                                               EZI2C_SIZE, I2CM_I2C_MODE_COMPLETE_XFER);
                if(status == I2CM_I2C_MSTR_NO_ERROR)
                {
                    /* wait for read complete and no error */
                    do
                    {
                        status = I2CM_I2CMasterStatus();
                    } while((status & (I2CM_I2C_MSTAT_RD_CMPLT | I2CM_I2C_MSTAT_ERR_XFER)) == 0u);
                }
                else
                {
                    /* translate from I2CM_MasterReadBuf() error output to
                    *  I2CM_MasterStatus() error output */
                    status = (uint8)I2CM_I2C_MSTAT_ERR_XFER;
                }
            }
        }
        if(status & I2CM_I2C_MSTAT_ERR_XFER)
        {
            /* add error handler code here */
        }

        /* SW2: I2C Master updates EZI2C data (RW portion only) */
        if(buttons & SW2)
        {
            /* Double the reload value, to slow down LEDs change rate */
            if(i2cMasterData.i2cMasterBuffer.reload < RELOAD_MAX)
            { /* don't overflow */
                i2cMasterData.i2cMasterBuffer.reload *= 2;
            }
            else
            { /* reinitialize */
                i2cMasterData.i2cMasterBuffer.reload = RELOAD_INIT;
            }
            /* Write data to the EZI2C (RW portion only).
            *  When writing to EZI2C, write the offset byte followed by the write data.
            */
            status = I2C_MasterWriteToEZI2C(EZI2C_SLAVE_ADDR, 1 + EZI2C_RW_SIZE);
            if(status & I2CM_I2C_MSTAT_ERR_XFER)
            {
                /* error handler goes here*/
            }
        }
        
        /* Display last byte of master buffer on RGB LEDs.
        *  Active low: 0 turns on the LED.
        */
        LEDs_Write(~(uint8)(i2cMasterData.i2cMasterBuffer.leds));
    } /* end of main loop */
} /* end of main() */


/*******************************************************************************
* Function Name: I2CMasterWriteToEZI2C
********************************************************************************
*
* Summary:
*  Writes from the I2C master (global struct i2cMasterData) to the EZI2C buffer.
*  This function is blocking: it writes with mode I2CM_I2C_MODE_COMPLETE_XFER,
*  and waits until I2CM_I2C_MSTAT_RD_CMPLT is set.
*
* Parameters:
*  1. i2cAddr: EZI2C slave I2C address
*  2. nbytes: # of bytes to write
*
* Return:
*  None
*
*******************************************************************************/
uint8 I2C_MasterWriteToEZI2C(uint8 i2CAddr, uint8 nbytes)
{
    uint8 volatile status;
    
    status = I2CM_I2CMasterClearStatus();
    if(!(status & I2CM_I2C_MSTAT_ERR_XFER))
    {
        status = I2CM_I2CMasterWriteBuf(i2CAddr, (uint8 *)&i2cMasterData, nbytes,
                                        I2CM_I2C_MODE_COMPLETE_XFER);
        if(status == I2CM_I2C_MSTR_NO_ERROR)
        {
            /* wait for write complete and no error */
            do
            {
                status = I2CM_I2CMasterStatus();
            } while((status & (I2CM_I2C_MSTAT_WR_CMPLT | I2CM_I2C_MSTAT_ERR_XFER)) == 0u);
        }
        else
        {
            /* translate from I2CM_I2CMasterWriteBuf() error output to
            *  I2CM_I2CMasterStatus() error output */
            status = (uint8)I2CM_I2C_MSTAT_ERR_XFER;
        }
    }
    
    return status;
}

/* [] END OF FILE */
