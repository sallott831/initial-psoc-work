/*******************************************************************************
* File Name: .h
* Version 1.10
*
* Description:
*  This private file provides constants and parameter values for the
*  SCB Component in I2C mode.
*  Please do not use this file or its content in your project.
*
* Note:
*
********************************************************************************
* Copyright 2013, Cypress Semiconductor Corporation. All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_SCB_PVT_I2C_M_H)
#define CY_SCB_PVT_I2C_M_H

#include "I2C_M.h"


/***************************************
*     Private Function Prototypes
***************************************/

/* APIs to service INTR_I2C_EC register */
#define I2C_M_SetI2CExtClkInterruptMode(interruptMask) I2C_M_WRITE_INTR_I2C_EC_MASK(interruptMask)
#define I2C_M_ClearI2CExtClkInterruptSource(interruptMask) I2C_M_CLEAR_INTR_I2C_EC(interruptMask)
#define I2C_M_GetI2CExtClkInterruptSource()                (I2C_M_INTR_I2C_EC_REG)
#define I2C_M_GetI2CExtClkInterruptMode()                  (I2C_M_INTR_I2C_EC_MASK_REG)
#define I2C_M_GetI2CExtClkInterruptSourceMasked()          (I2C_M_INTR_I2C_EC_MASKED_REG)

/* APIs to service INTR_SPI_EC register */
#define I2C_M_SetSpiExtClkInterruptMode(interruptMask) I2C_M_WRITE_INTR_SPI_EC_MASK(interruptMask)
#define I2C_M_ClearSpiExtClkInterruptSource(interruptMask) I2C_M_CLEAR_INTR_SPI_EC(interruptMask)
#define I2C_M_GetExtSpiClkInterruptSource()                 (I2C_M_INTR_SPI_EC_REG)
#define I2C_M_GetExtSpiClkInterruptMode()                   (I2C_M_INTR_SPI_EC_MASK_REG)
#define I2C_M_GetExtSpiClkInterruptSourceMasked()           (I2C_M_INTR_SPI_EC_MASKED_REG)

#if(I2C_M_SCB_MODE_UNCONFIG_CONST_CFG)
    extern void I2C_M_SetPins(uint32 mode, uint32 subMode, uint32 uartTxRx);
#endif /* (I2C_M_SCB_MODE_UNCONFIG_CONST_CFG) */

void I2C_M_DisableTxPinsInputBuffer(void);
void I2C_M_EnableTxPinsInputBuffer(void);


/**********************************
*     Vars with External Linkage
**********************************/

extern cyisraddress I2C_M_customIntrHandler;
extern I2C_M_BACKUP_STRUCT I2C_M_backup;

#if(I2C_M_SCB_MODE_UNCONFIG_CONST_CFG)
    /* Common config vars */
    extern uint8 I2C_M_scbMode;
    extern uint8 I2C_M_scbEnableWake;
    extern uint8 I2C_M_scbEnableIntr;

    /* I2C config vars */
    extern uint8 I2C_M_mode;
    extern uint8 I2C_M_acceptAddr;

    /* SPI/UART config vars */
    extern volatile uint8 * I2C_M_rxBuffer;
    extern uint8   I2C_M_rxDataBits;
    extern uint32  I2C_M_rxBufferSize;

    extern volatile uint8 * I2C_M_txBuffer;
    extern uint8   I2C_M_txDataBits;
    extern uint32  I2C_M_txBufferSize;

    /* EZI2C config vars */
    extern uint8 I2C_M_numberOfAddr;
    extern uint8 I2C_M_subAddrSize;
#endif /* (I2C_M_SCB_MODE_UNCONFIG_CONST_CFG) */

#endif /* (CY_SCB_PVT_I2C_M_H) */


/* [] END OF FILE */
