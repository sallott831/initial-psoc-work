/*******************************************************************************
* File Name: .h
* Version 1.10
*
* Description:
*  This private file provides constants and parameter values for the
*  SCB Component in I2C mode.
*  Please do not use this file or its content in your project.
*
* Note:
*
********************************************************************************
* Copyright 2013, Cypress Semiconductor Corporation. All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_SCB_PVT_I2C_S_H)
#define CY_SCB_PVT_I2C_S_H

#include "I2C_S.h"


/***************************************
*     Private Function Prototypes
***************************************/

/* APIs to service INTR_I2C_EC register */
#define I2C_S_SetI2CExtClkInterruptMode(interruptMask) I2C_S_WRITE_INTR_I2C_EC_MASK(interruptMask)
#define I2C_S_ClearI2CExtClkInterruptSource(interruptMask) I2C_S_CLEAR_INTR_I2C_EC(interruptMask)
#define I2C_S_GetI2CExtClkInterruptSource()                (I2C_S_INTR_I2C_EC_REG)
#define I2C_S_GetI2CExtClkInterruptMode()                  (I2C_S_INTR_I2C_EC_MASK_REG)
#define I2C_S_GetI2CExtClkInterruptSourceMasked()          (I2C_S_INTR_I2C_EC_MASKED_REG)

/* APIs to service INTR_SPI_EC register */
#define I2C_S_SetSpiExtClkInterruptMode(interruptMask) I2C_S_WRITE_INTR_SPI_EC_MASK(interruptMask)
#define I2C_S_ClearSpiExtClkInterruptSource(interruptMask) I2C_S_CLEAR_INTR_SPI_EC(interruptMask)
#define I2C_S_GetExtSpiClkInterruptSource()                 (I2C_S_INTR_SPI_EC_REG)
#define I2C_S_GetExtSpiClkInterruptMode()                   (I2C_S_INTR_SPI_EC_MASK_REG)
#define I2C_S_GetExtSpiClkInterruptSourceMasked()           (I2C_S_INTR_SPI_EC_MASKED_REG)

#if(I2C_S_SCB_MODE_UNCONFIG_CONST_CFG)
    extern void I2C_S_SetPins(uint32 mode, uint32 subMode, uint32 uartTxRx);
#endif /* (I2C_S_SCB_MODE_UNCONFIG_CONST_CFG) */

void I2C_S_DisableTxPinsInputBuffer(void);
void I2C_S_EnableTxPinsInputBuffer(void);


/**********************************
*     Vars with External Linkage
**********************************/

extern cyisraddress I2C_S_customIntrHandler;
extern I2C_S_BACKUP_STRUCT I2C_S_backup;

#if(I2C_S_SCB_MODE_UNCONFIG_CONST_CFG)
    /* Common config vars */
    extern uint8 I2C_S_scbMode;
    extern uint8 I2C_S_scbEnableWake;
    extern uint8 I2C_S_scbEnableIntr;

    /* I2C config vars */
    extern uint8 I2C_S_mode;
    extern uint8 I2C_S_acceptAddr;

    /* SPI/UART config vars */
    extern volatile uint8 * I2C_S_rxBuffer;
    extern uint8   I2C_S_rxDataBits;
    extern uint32  I2C_S_rxBufferSize;

    extern volatile uint8 * I2C_S_txBuffer;
    extern uint8   I2C_S_txDataBits;
    extern uint32  I2C_S_txBufferSize;

    /* EZI2C config vars */
    extern uint8 I2C_S_numberOfAddr;
    extern uint8 I2C_S_subAddrSize;
#endif /* (I2C_S_SCB_MODE_UNCONFIG_CONST_CFG) */

#endif /* (CY_SCB_PVT_I2C_S_H) */


/* [] END OF FILE */
