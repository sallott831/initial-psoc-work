/*******************************************************************************
* File Name: SW1_Write.h  
* Version 1.90
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_SW1_Write_H) /* Pins SW1_Write_H */
#define CY_PINS_SW1_Write_H

#include "cytypes.h"
#include "cyfitter.h"
#include "SW1_Write_aliases.h"


/***************************************
*        Function Prototypes             
***************************************/    

void    SW1_Write_Write(uint8 value) ;
void    SW1_Write_SetDriveMode(uint8 mode) ;
uint8   SW1_Write_ReadDataReg(void) ;
uint8   SW1_Write_Read(void) ;
uint8   SW1_Write_ClearInterrupt(void) ;


/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define SW1_Write_DRIVE_MODE_BITS        (3)
#define SW1_Write_DRIVE_MODE_IND_MASK    (0xFFFFFFFFu >> (32 - SW1_Write_DRIVE_MODE_BITS))
#define SW1_Write_DRIVE_MODE_SHIFT       (0x00u)
#define SW1_Write_DRIVE_MODE_MASK        (0x07u << SW1_Write_DRIVE_MODE_SHIFT)

#define SW1_Write_DM_ALG_HIZ         (0x00u << SW1_Write_DRIVE_MODE_SHIFT)
#define SW1_Write_DM_DIG_HIZ         (0x01u << SW1_Write_DRIVE_MODE_SHIFT)
#define SW1_Write_DM_RES_UP          (0x02u << SW1_Write_DRIVE_MODE_SHIFT)
#define SW1_Write_DM_RES_DWN         (0x03u << SW1_Write_DRIVE_MODE_SHIFT)
#define SW1_Write_DM_OD_LO           (0x04u << SW1_Write_DRIVE_MODE_SHIFT)
#define SW1_Write_DM_OD_HI           (0x05u << SW1_Write_DRIVE_MODE_SHIFT)
#define SW1_Write_DM_STRONG          (0x06u << SW1_Write_DRIVE_MODE_SHIFT)
#define SW1_Write_DM_RES_UPDWN       (0x07u << SW1_Write_DRIVE_MODE_SHIFT)

/* Digital Port Constants */
#define SW1_Write_MASK               SW1_Write__MASK
#define SW1_Write_SHIFT              SW1_Write__SHIFT
#define SW1_Write_WIDTH              1u


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define SW1_Write_PS                     (* (reg32 *) SW1_Write__PS)
/* Port Configuration */
#define SW1_Write_PC                     (* (reg32 *) SW1_Write__PC)
/* Data Register */
#define SW1_Write_DR                     (* (reg32 *) SW1_Write__DR)
/* Input Buffer Disable Override */
#define SW1_Write_INP_DIS                (* (reg32 *) SW1_Write__PC2)


#if defined(SW1_Write__INTSTAT)  /* Interrupt Registers */

    #define SW1_Write_INTSTAT                (* (reg32 *) SW1_Write__INTSTAT)

#endif /* Interrupt Registers */

#endif /* End Pins SW1_Write_H */


/* [] END OF FILE */
