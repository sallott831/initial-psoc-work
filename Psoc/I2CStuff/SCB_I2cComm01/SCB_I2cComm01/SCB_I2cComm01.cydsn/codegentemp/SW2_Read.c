/*******************************************************************************
* File Name: SW2_Read.c  
* Version 1.90
*
* Description:
*  This file contains API to enable firmware control of a Pins component.
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "cytypes.h"
#include "SW2_Read.h"

#define SetP4PinDriveMode(shift, mode)  \
    do { \
        SW2_Read_PC =   (SW2_Read_PC & \
                                (uint32)(~(uint32)(SW2_Read_DRIVE_MODE_IND_MASK << (SW2_Read_DRIVE_MODE_BITS * (shift))))) | \
                                (uint32)((uint32)(mode) << (SW2_Read_DRIVE_MODE_BITS * (shift))); \
    } while (0)


/*******************************************************************************
* Function Name: SW2_Read_Write
********************************************************************************
*
* Summary:
*  Assign a new value to the digital port's data output register.  
*
* Parameters:  
*  prtValue:  The value to be assigned to the Digital Port. 
*
* Return: 
*  None 
*  
*******************************************************************************/
void SW2_Read_Write(uint8 value) 
{
    uint8 drVal = (uint8)(SW2_Read_DR & (uint8)(~SW2_Read_MASK));
    drVal = (drVal | ((uint8)(value << SW2_Read_SHIFT) & SW2_Read_MASK));
    SW2_Read_DR = (uint32)drVal;
}


/*******************************************************************************
* Function Name: SW2_Read_SetDriveMode
********************************************************************************
*
* Summary:
*  Change the drive mode on the pins of the port.
* 
* Parameters:  
*  mode:  Change the pins to this drive mode.
*
* Return: 
*  None
*
*******************************************************************************/
void SW2_Read_SetDriveMode(uint8 mode) 
{
	SetP4PinDriveMode(SW2_Read__0__SHIFT, mode);
}


/*******************************************************************************
* Function Name: SW2_Read_Read
********************************************************************************
*
* Summary:
*  Read the current value on the pins of the Digital Port in right justified 
*  form.
*
* Parameters:  
*  None 
*
* Return: 
*  Returns the current value of the Digital Port as a right justified number
*  
* Note:
*  Macro SW2_Read_ReadPS calls this function. 
*  
*******************************************************************************/
uint8 SW2_Read_Read(void) 
{
    return (uint8)((SW2_Read_PS & SW2_Read_MASK) >> SW2_Read_SHIFT);
}


/*******************************************************************************
* Function Name: SW2_Read_ReadDataReg
********************************************************************************
*
* Summary:
*  Read the current value assigned to a Digital Port's data output register
*
* Parameters:  
*  None 
*
* Return: 
*  Returns the current value assigned to the Digital Port's data output register
*  
*******************************************************************************/
uint8 SW2_Read_ReadDataReg(void) 
{
    return (uint8)((SW2_Read_DR & SW2_Read_MASK) >> SW2_Read_SHIFT);
}


/* If Interrupts Are Enabled for this Pins component */ 
#if defined(SW2_Read_INTSTAT) 

    /*******************************************************************************
    * Function Name: SW2_Read_ClearInterrupt
    ********************************************************************************
    *
    * Summary:
    *  Clears any active interrupts attached to port and returns the value of the 
    *  interrupt status register.
    *
    * Parameters:  
    *  None 
    *
    * Return: 
    *  Returns the value of the interrupt status register
    *  
    *******************************************************************************/
    uint8 SW2_Read_ClearInterrupt(void) 
    {
		uint8 maskedStatus = (uint8)(SW2_Read_INTSTAT & SW2_Read_MASK);
		SW2_Read_INTSTAT = maskedStatus;
        return maskedStatus >> SW2_Read_SHIFT;
    }

#endif /* If Interrupts Are Enabled for this Pins component */ 


/* [] END OF FILE */
