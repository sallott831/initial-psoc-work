/*******************************************************************************
* File Name: SW2_Read.h  
* Version 1.90
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_SW2_Read_H) /* Pins SW2_Read_H */
#define CY_PINS_SW2_Read_H

#include "cytypes.h"
#include "cyfitter.h"
#include "SW2_Read_aliases.h"


/***************************************
*        Function Prototypes             
***************************************/    

void    SW2_Read_Write(uint8 value) ;
void    SW2_Read_SetDriveMode(uint8 mode) ;
uint8   SW2_Read_ReadDataReg(void) ;
uint8   SW2_Read_Read(void) ;
uint8   SW2_Read_ClearInterrupt(void) ;


/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define SW2_Read_DRIVE_MODE_BITS        (3)
#define SW2_Read_DRIVE_MODE_IND_MASK    (0xFFFFFFFFu >> (32 - SW2_Read_DRIVE_MODE_BITS))
#define SW2_Read_DRIVE_MODE_SHIFT       (0x00u)
#define SW2_Read_DRIVE_MODE_MASK        (0x07u << SW2_Read_DRIVE_MODE_SHIFT)

#define SW2_Read_DM_ALG_HIZ         (0x00u << SW2_Read_DRIVE_MODE_SHIFT)
#define SW2_Read_DM_DIG_HIZ         (0x01u << SW2_Read_DRIVE_MODE_SHIFT)
#define SW2_Read_DM_RES_UP          (0x02u << SW2_Read_DRIVE_MODE_SHIFT)
#define SW2_Read_DM_RES_DWN         (0x03u << SW2_Read_DRIVE_MODE_SHIFT)
#define SW2_Read_DM_OD_LO           (0x04u << SW2_Read_DRIVE_MODE_SHIFT)
#define SW2_Read_DM_OD_HI           (0x05u << SW2_Read_DRIVE_MODE_SHIFT)
#define SW2_Read_DM_STRONG          (0x06u << SW2_Read_DRIVE_MODE_SHIFT)
#define SW2_Read_DM_RES_UPDWN       (0x07u << SW2_Read_DRIVE_MODE_SHIFT)

/* Digital Port Constants */
#define SW2_Read_MASK               SW2_Read__MASK
#define SW2_Read_SHIFT              SW2_Read__SHIFT
#define SW2_Read_WIDTH              1u


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define SW2_Read_PS                     (* (reg32 *) SW2_Read__PS)
/* Port Configuration */
#define SW2_Read_PC                     (* (reg32 *) SW2_Read__PC)
/* Data Register */
#define SW2_Read_DR                     (* (reg32 *) SW2_Read__DR)
/* Input Buffer Disable Override */
#define SW2_Read_INP_DIS                (* (reg32 *) SW2_Read__PC2)


#if defined(SW2_Read__INTSTAT)  /* Interrupt Registers */

    #define SW2_Read_INTSTAT                (* (reg32 *) SW2_Read__INTSTAT)

#endif /* Interrupt Registers */

#endif /* End Pins SW2_Read_H */


/* [] END OF FILE */
