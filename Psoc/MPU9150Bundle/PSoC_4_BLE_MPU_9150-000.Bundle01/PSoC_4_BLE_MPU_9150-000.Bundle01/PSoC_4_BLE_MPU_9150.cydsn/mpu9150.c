/************************************************************************************************************************
* Project Name		: PSoC_4_BLE_Pressure_Sensor
* File Name			: I2CMaster.c
* Version 			: 1.0
* Device Used		: CY8C4247LQI-BL483
* Software Used		: PSoC Creator 3.1
* Compiler    		: ARM GCC 4.8.4
* Related Hardware	: CY8CKIT-042-BLE Bluetooth Low Energy Pioneer Kit
*************************************************************************************************************************/

/************************************************************************************************************************
* Copyright (2015), Cypress Semiconductor Corporation. All Rights Reserved.
************************************************************************************************************************
* This software, including source code, documentation and related materials
* ("Software") is owned by Cypress Semiconductor Corporation (Cypress) and is
* protected by and subject to worldwide patent protection (United States and 
* foreign), United States copyright laws and international treaty provisions. 
* Cypress hereby grants to licensee a personal, non-exclusive, non-transferable
* license to copy, use, modify, create derivative works of, and compile the 
* Cypress source code and derivative works for the sole purpose of creating 
* custom software in support of licensee product, such licensee product to be
* used only in conjunction with Cypress's integrated circuit as specified in the
* applicable agreement. Any reproduction, modification, translation, compilation,
* or representation of this Software except as specified above is prohibited 
* without the express written permission of Cypress.
* 
* Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO WARRANTY OF ANY KIND, 
* EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED 
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
* Cypress reserves the right to make changes to the Software without notice. 
* Cypress does not assume any liability arising out of the application or use
* of Software or any product or circuit described in the Software. Cypress does
* not authorize its products for use as critical components in any products 
* where a malfunction or failure may reasonably be expected to result in 
* significant injury or death ("ACTIVE Risk Product"). By including Cypress's 
* product in a ACTIVE Risk Product, the manufacturer of such system or application
* assumes all risk of such use and in doing so indemnifies Cypress against all
* liability. Use of this Software may be limited by and subject to the applicable
* Cypress software license agreement.
***********************************************************************************************************************/

#include <mpu9150.h>
#include <mpu6050.h>

/*************************Variables Declaration*************************************************************************/
uint8 SensorCalibrationData[0x16]; //Calibration data is stored in this array
long X1, X2, X3, B3, B5, B6, UT, UP; //Variables to store calibration coefficients
unsigned long B4, B7; //Variables to store calibration coefficients
short AC1, AC2, AC3, B1, B2, MC, MD; //Variables to store calibration coefficients
unsigned short AC4, AC5, AC6; //Variables to store calibration coefficients
long UT, UP; //Variables to store uncompensated temperature and pressure data
long Temperature, Pressure, Altitude; //Variables to store the true temperature, pressure and altitude data
double PressureAtSeaLevel = 101325.0; //Pressure at sea level in Pascal
uint8 CalibrationDataAvailable = FALSE; //This flag is to whether the calibration data is available
/***********************************************************************************************************************/


/***********************************************************************************************************************/
/*************************************************************************************************************************
* Function Name: I2C_reg_write8
**************************************************************************************************************************
* Summary: This function writes an 8byte value (reg_data) into a register(reg_addr) of an I2C slave device (dev_addr)
*
* Parameters:
*  uint8 dev_addr, uint8 reg_addr, uint8 reg_data
*
* Return:
*  void
*
*************************************************************************************************************************/
void I2C_reg_write8(uint8 dev_addr, uint8 reg_addr, uint8 reg_data  )
{
  {
    static uint8 I2CWriteBuffer[0x02]; //Register address and value
    
    I2CWriteBuffer[0x00] = reg_addr;
    I2CWriteBuffer[0x01] = reg_data;
    
    /* Do a write operation with the memory address bytes to get temperature data */
	I2C_I2CMasterWriteBuf(dev_addr, (uint8 *) I2CWriteBuffer, sizeof(I2CWriteBuffer), I2C_I2C_MODE_COMPLETE_XFER);
    while(!(I2C_I2CMasterStatus() & I2C_I2C_MSTAT_WR_CMPLT)); //Wait till the master completes writing
    I2C_I2CMasterClearStatus(); //Clear I2C master status
    
}
}

/***********************************************************************************************************************/  
/*************************************************************************************************************************
* Function Name: I2C_reg_read8
**************************************************************************************************************************
* Summary: This function returns an 8bit (1 byte) value read from register(reg_addr) of an I2C slave device (dev_addr)
*
* Parameters:
*  uint8 dev_addr, uint8 reg_addr
*
* Return:
*  void
*
*************************************************************************************************************************/
uint8 I2C_reg_read8(uint8 dev_addr, uint8 reg_addr)
{
    static uint8 I2CReadBuffer[0x01]; //The value read from register is placed here
    static uint8 SetReadAddressBuffer[0x01]; //Register address from which the data is read
    
   SetReadAddressBuffer[0x00] = reg_addr;
    
    /* In order to do a read from a specific address, do a write operation with the memory address bytes alone */
	I2C_I2CMasterWriteBuf(dev_addr, (uint8 *) SetReadAddressBuffer, sizeof(SetReadAddressBuffer), I2C_I2C_MODE_COMPLETE_XFER);
    while(!(I2C_I2CMasterStatus() & I2C_I2C_MSTAT_WR_CMPLT)); //Wait till the master completes writing
    I2C_I2CMasterClearStatus(); //Clear I2C master status
    
    /* Read temperature from the sensor and store it in a buffer */
	I2C_I2CMasterReadBuf(dev_addr, (uint8 *) I2CReadBuffer, sizeof(I2CReadBuffer), I2C_I2C_MODE_COMPLETE_XFER);
    while(!(I2C_I2CMasterStatus() & I2C_I2C_MSTAT_RD_CMPLT)); //Wait till the master completes reading
    I2C_I2CMasterClearStatus(); //Clear I2C master status
    
    return I2CReadBuffer[0x00];
}

/***********************************************************************************************************************/
    
/*************************************************************************************************************************
* Function Name: I2C_set_register_bits
**************************************************************************************************************************
* Summary: This function sets the corresponding bits of a register at (reg_addr) by first reading the value and the
*           then or'ing (bitfield) on the value and then writing it back
*
* Parameters:
*  uint8 dev_addr, uint8 reg_addr, uint8 reg_data
*
* Return:
*  void
*
*************************************************************************************************************************/
void I2C_set_register_bits(uint8 dev_addr, uint8 reg_addr, uint8 bitstart, uint8 length, uint8 bitfield  )
{
  {
    uint8 register_data;
        
    register_data = I2C_reg_read8(dev_addr,reg_addr);
    uint8 mask = ((1 << length) - 1) << (bitstart - length +1);
    bitfield <<= (bitstart - length + 1); //shift bitfield data to correct position
    bitfield &= mask;  // zero out other bits
    register_data &= ~(mask); // zero all bitfield position bits in existing byte
    register_data |= bitfield;  // 'or' in the bitfield bits into the register data field    
    I2C_reg_write8(dev_addr,reg_addr,register_data);
    return;
    
}
}void I2C_set_register_bit(uint8 dev_addr, uint8 reg_addr, uint8 bit_number, uint8 bit  )
{
  {
    uint8 register_data;
        
    register_data = I2C_reg_read8(dev_addr,reg_addr); //initial value of register
    register_data = (bit != 0) ? (register_data | (1 << bit_number)) : (register_data & ~(1 << bit_number));
    
    I2C_reg_write8(dev_addr,reg_addr,register_data);
    return;
    
}
}
/***********************************************************************************************************************/

/*************************************************************************************************************************
* Function Name: MPU9150_Initialize
**************************************************************************************************************************
* Summary: This function reads the sensor calibration data over I2C and
* assigns it to the appropriate sensor coefficient variables.
*
* Parameters:
*  void
*
* Return:
*  void
*
*************************************************************************************************************************/
void MPU9150_Initialize(void)
{
    I2C_set_register_bits(MPU9150_Addr, MPU6050_RA_PWR_MGMT_1,MPU6050_PWR1_CLKSEL_BIT,MPU6050_PWR1_CLKSEL_LENGTH,MPU6050_CLOCK_PLL_XGYRO);
//    setClockSource(MPU6050_CLOCK_PLL_XGYRO);
    I2C_set_register_bits(MPU9150_Addr, MPU6050_RA_GYRO_CONFIG,MPU6050_GCONFIG_FS_SEL_BIT,MPU6050_GCONFIG_FS_SEL_LENGTH,MPU6050_GYRO_FS_250);
//    setFullScaleGyroRange(MPU6050_GYRO_FS_250);
    I2C_set_register_bits(MPU9150_Addr, MPU6050_RA_ACCEL_CONFIG, MPU6050_ACONFIG_AFS_SEL_BIT, MPU6050_ACONFIG_AFS_SEL_LENGTH, MPU6050_ACCEL_FS_2);
//    setFullScaleAccelRange(MPU6050_ACCEL_FS_2);
    I2C_set_register_bit(MPU9150_Addr,  MPU6050_RA_PWR_MGMT_1, MPU6050_PWR1_SLEEP_BIT, FALSE);
//    setSleepEnabled(FALSE);
    
}


/*************************************************************************************************************************
* Function Name: ReadSensorCalibrationData
**************************************************************************************************************************
* Summary: This function reads the sensor calibration data over I2C and
* assigns it to the appropriate sensor coefficient variables.
*
* Parameters:
*  void
*
* Return:
*  void
*
*************************************************************************************************************************/
void ReadSensorCalibrationData(void)
{
    static uint8 I2CWriteBuffer[0x01] = {0xAA}; //Location from which the calibration data is to be read
    
    /* Do a write operation with the memory address bytes to get calibration data */
	I2C_I2CMasterWriteBuf(MPU9150_Addr, (uint8 *) I2CWriteBuffer, sizeof(I2CWriteBuffer), I2C_I2C_MODE_COMPLETE_XFER);
    while(!(I2C_I2CMasterStatus() & I2C_I2C_MSTAT_WR_CMPLT)); //Wait till the master completes writing
    I2C_I2CMasterClearStatus(); //Clear I2C master status
    
    /* Read calibration from the sensor EEPROM and store it in a buffer */
	I2C_I2CMasterReadBuf(MPU9150_Addr, (uint8 *) SensorCalibrationData, sizeof(SensorCalibrationData), I2C_I2C_MODE_COMPLETE_XFER);
    while(!(I2C_I2CMasterStatus() & I2C_I2C_MSTAT_RD_CMPLT)); //Wait till the master completes reading
    I2C_I2CMasterClearStatus(); //Clear I2C master status
    
    CalibrationDataAvailable = TRUE; //Set sensor calibration data available flag
    
    /* Assign calibration data to coefficient variables as described in sensor datasheet */
    AC1 = (SensorCalibrationData[0x00] << 0x08) | SensorCalibrationData[0x01];
    AC2 = (SensorCalibrationData[0x02] << 0x08) | SensorCalibrationData[0x03];
    AC3 = (SensorCalibrationData[0x04] << 0x08) | SensorCalibrationData[0x05];
    AC4 = (SensorCalibrationData[0x06] << 0x08) | SensorCalibrationData[0x07];
    AC5 = (SensorCalibrationData[0x08] << 0x08) | SensorCalibrationData[0x09];
    AC6 = (SensorCalibrationData[0x0A] << 0x08) | SensorCalibrationData[0x0B];
    B1 = (SensorCalibrationData[0x0C] << 0x08) | SensorCalibrationData[0x0D];
    B2 = (SensorCalibrationData[0x0E] << 0x08) | SensorCalibrationData[0x0F];
    MC = (SensorCalibrationData[0x012] << 0x08) | SensorCalibrationData[0x013];
    MD = (SensorCalibrationData[0x014] << 0x08) | SensorCalibrationData[0x015];
}
/***********************************************************************************************************************/

/*************************************************************************************************************************
* Function Name: ReadUncompensatedTemperature
**************************************************************************************************************************
* Summary: This function writes the temperature conversion command to the sensor and
* waits for conversion to complete, and then reads the converted data (uncompensated temperature) and stores it in a variable.
*
* Parameters:
*  void
*
* Return:
*  void
*
*************************************************************************************************************************/
void ReadUncompensatedTemperature(void)
{
    static uint8 I2CReadBuffer[0x02]; //The uncompensated temperature read from the sensor is stored here
    static uint8 I2CWriteBuffer[0x02] = {0xF4, 0x2E}; //Command for temperature conversion - Register address and value
    static uint8 SetReadAddressBuffer[0x01] = {0xF6}; //Register address from which the data is read
    
    /* Do a write operation with the memory address bytes to get temperature data */
	I2C_I2CMasterWriteBuf(MPU9150_Addr, (uint8 *) I2CWriteBuffer, sizeof(I2CWriteBuffer), I2C_I2C_MODE_COMPLETE_XFER);
    while(!(I2C_I2CMasterStatus() & I2C_I2C_MSTAT_WR_CMPLT)); //Wait till the master completes writing
    I2C_I2CMasterClearStatus(); //Clear I2C master status
    
    CyDelay(5); //Wait for 5ms temperature conversion
    
    /* In order to do a read from a specific address, do a write operation with the memory address bytes alone */
	I2C_I2CMasterWriteBuf(MPU9150_Addr, (uint8 *) SetReadAddressBuffer, sizeof(SetReadAddressBuffer), I2C_I2C_MODE_COMPLETE_XFER);
    while(!(I2C_I2CMasterStatus() & I2C_I2C_MSTAT_WR_CMPLT)); //Wait till the master completes writing
    I2C_I2CMasterClearStatus(); //Clear I2C master status
    
    /* Read temperature from the sensor and store it in a buffer */
	I2C_I2CMasterReadBuf(MPU9150_Addr, (uint8 *) I2CReadBuffer, sizeof(I2CReadBuffer), I2C_I2C_MODE_COMPLETE_XFER);
    while(!(I2C_I2CMasterStatus() & I2C_I2C_MSTAT_RD_CMPLT)); //Wait till the master completes reading
    I2C_I2CMasterClearStatus(); //Clear I2C master status
    
	UT = (I2CReadBuffer[0x00] << 0x08) | I2CReadBuffer[0x01]; //Return uncompensated temperature value
}
/***********************************************************************************************************************/

/*************************************************************************************************************************
* Function Name: ReadUncompensatedPressure
**************************************************************************************************************************
* Summary: This function writes the pressure conversion command to the sensor and
* waits for conversion to complete, and then reads the converted data (uncompensated pressure) and stores it in a variable.
*
* Parameters:
*  void
*
* Return:
*  void
*
*************************************************************************************************************************/
void ReadUncompensatedPressure(void)
{
    static uint8 I2CReadBuffer[0x03]; //The uncompensated pressure read from the sensor is stored here
    static uint8 I2CWriteBuffer[0x02] = {0xF4, 0x34 + (OSS << 0x06)}; //Command for pressure conversion - Register address and value
    static uint8 SetReadAddressBuffer[0x01] = {0xF6}; //Register address from which the data is read
    
    /* Do a write operation with the memory address bytes to get pressure data */
	I2C_I2CMasterWriteBuf(MPU9150_Addr, (uint8 *) I2CWriteBuffer, sizeof(I2CWriteBuffer), I2C_I2C_MODE_COMPLETE_XFER);
    while(!(I2C_I2CMasterStatus() & I2C_I2C_MSTAT_WR_CMPLT)); //Wait till the master completes writing
    I2C_I2CMasterClearStatus(); //Clear I2C master status
    
    CyDelay(26); //Wait for 26ms pressure conversion
    
    /* In order to do a read from a specific address, do a write operation with the memory address bytes alone */
	I2C_I2CMasterWriteBuf(MPU9150_Addr, (uint8 *) SetReadAddressBuffer, sizeof(SetReadAddressBuffer), I2C_I2C_MODE_COMPLETE_XFER);
    while(!(I2C_I2CMasterStatus() & I2C_I2C_MSTAT_WR_CMPLT)); //Wait till the master completes writing
    I2C_I2CMasterClearStatus(); //Clear I2C master status
    
    /* Read pressure from the sensor and store it in a buffer */
	I2C_I2CMasterReadBuf(MPU9150_Addr, (uint8 *) I2CReadBuffer, sizeof(I2CReadBuffer), I2C_I2C_MODE_COMPLETE_XFER);
    while(!(I2C_I2CMasterStatus() & I2C_I2C_MSTAT_RD_CMPLT)); //Wait till the master completes reading
    I2C_I2CMasterClearStatus(); //Clear I2C master status
    
	UP = ((I2CReadBuffer[0x00] << 0x10) | (I2CReadBuffer[0x01] << 0x08) | I2CReadBuffer[0x02]) >> (0x08 - OSS); //Return uncompensated pressure value
}
/***********************************************************************************************************************/

/*************************************************************************************************************************
* Function Name: ReadCalculateSensorData
**************************************************************************************************************************
* Summary: This function reads the uncompensated temperature and pressure value and converts it
* into true temperature, pressure and altitude using the algorithm given in the sensor datasheet.
*
* Parameters:
*  void
*
* Return:
*  void
*
*************************************************************************************************************************/
void ReadAndCalculateSensorData(void)
{
    /* Read the sensor calibration data if not already available */
	if(!CalibrationDataAvailable)
    {
        ReadSensorCalibrationData(); //Read sensor calibration data
    }
    
    ReadUncompensatedTemperature(); //Read the uncompensated temperature value
    ReadUncompensatedPressure(); //Read the uncompensated pressure value
    
    /* Calculate the true temperature as described in sensor datasheet */
    X1 = (UT - AC6) * AC5 / pow(2, 15);
    X2 = MC * pow(2, 11) / (X1 + MD);
    B5 = X1 + X2;
    Temperature = (B5 + 8) / pow(2, 4);
    
    /* Calculate the true barometric pressure as described in sensor datasheet */
    B6 = B5 - 4000;
    X1 = (B2 * (B6 * B6 / pow(2, 12))) / pow(2, 11);
    X2 = AC2 * B6 / pow(2, 11);
    X3 = X1 + X2;
    B3 = (((AC1 * 4 + X3) << OSS) + 2) / 4;
    X1 = AC3 * B6 / pow(2, 13);
    X2 = (B1 * (B6 * B6 / pow(2, 12))) / pow(2, 16);
    X3 = ((X1 + X2) + 2) / pow(2, 2);
    B4 = (AC4 * (unsigned long)(X3 + 32768)) / pow(2, 15);
    B7 = ((unsigned long)UP - B3) * (50000 >> OSS);
    if(B7 < 0x80000000)
    {
        Pressure = (B7 *2) / B4;
    }
    else 
    {
        Pressure = (B7 / B4) *2;
    }
    X1 = (Pressure / pow(2, 8)) * (Pressure / pow(2, 8));
    X1 = (X1 * 3038) / pow(2, 16);
    X2 = (-7357 * Pressure) / pow(2, 16);
    Pressure = Pressure + (X1 + X2 + 3791) / pow(2, 4);
    
    /* Calculate the true altitude based on pressure at sea level as described in sensor datasheet */
    Altitude = 44330 * (1 - (pow(Pressure/PressureAtSeaLevel, 1/5.255)));
}
/***********************************************************************************************************************/

/* [] END OF FILE */
